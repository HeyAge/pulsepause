#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Pulse Pause.

Usage:
  pulsepause.py [--pulse=<min>] [--pause=<min>]
  pulsepause.py (-h | --help)
  pulsepause.py (-v | --version)

Options:
  -h --help     Show this screen.
  --version     Show version.
  --pulse=<iv>  Pulse in minutes [default: 90].
  --pause=<iv>  Pause in minutes [default: 20].

"""

# the above is our usage string that docopt will read and use to determine
# whether or not the user has passed valid arguments.

from sys import platform as _platform
from docopt import docopt
import time
import subprocess


def main():
    """Main-entry point for program.

    Expects dict with arguments from docopt()

    """
    args = docopt(__doc__, version='0.1')
    # Pulse duration in minutes.
    pulse = args["--pulse"]
    pause = args["--pause"]
    pulse = int(pulse)
    pause = int(pause)

    # Endless loop
    while 1:
        interval(pulse, "pulse")
        interval(pause, "pause")


def interval(minutes, pulsepause):
    """Set the minutes."""
    mins = 0
    while mins != minutes:
        print ">>>>>>>>>>>>>>>>>>>>>", mins
        # Sleep in seconds
        time.sleep(60)
        # Increment the minute total
        mins += 1

    pulse_pause(pulsepause)


def pulse_pause(pulsepause):
    """Decide whether pulse or pause."""

    # Bring up the dialog box here
    if pulsepause == "pulse":
        if _platform == "linux" or _platform == "linux2":
            # linux
            print "Pause"
        elif _platform == "darwin":
            cmd = """osascript -e 'display notification "Relax!" with title "Pause"'"""
            subprocess.call(cmd, shell=True)
        elif _platform == win32 or _platform == "cygwin":
            # windows box will appear ot top of other windows
            win32api.MessageBox(0, 'hello', 'title', 0x00001000)
    elif pulsepause == "pause":
        if _platform == "linux" or _platform == "linux2":
            # linux
            print "Pulse"
        elif _platform == "darwin":
        # os x
            cmd = """osascript -e 'display notification "Work!" with title "Pulse"'"""
            subprocess.call(cmd, shell=True)
        elif _platform == win32 or _platform == "cygwin":
            # windows box will appear ot top of other windows
            win32api.MessageBox(0, 'hello', 'title', 0x00001000)

if __name__ == '__main__':
    # Docopt will check all arguments, and exit with the Usage string if they
    # don't pass.
    # If you simply want to pass your own modules documentation
    # then use __doc__,
    # otherwise, you would pass another docopt-friendly usage string here.
    # You could also pass your own arguments instead of sys.argv with:
    # docopt(__doc__, argv=[your, args])
    # run = raw_input("Start? > ")

    # We have valid args, so run the program
    main()
