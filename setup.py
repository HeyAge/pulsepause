from setuptools import setup, find_packages
import os

version = '0.1'

setup(name='Focus',
      version=version,
      description="Timer for focused working.",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='',
      author='Helder Fernandes',
      author_email='helder.fernandes@itverde.com',
      url='http://svn.plone.org/svn/collective/',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['focus'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points={
        'console_scripts': [
            'pulse_pause = focus.pulse_pause:main',
            ]
        }
      )
